class PagesController < ApplicationController
    # GET /pages/new
    def new
        @page = Page.new
        if params.has_key?(:id)
            @page.title =  params[:id]
        end
    end

    # GET /page
    def index
        @pages = Page.all
    end

    def show
        if Page.friendly.where(:slug => params[:id].downcase).blank?
            redirect_to  :action => 'new', :id => params[:id]
        else
            @page = Page.friendly.find(params[:id].downcase)
        end
    end

    # POST /page
    def create
        @page = Page.new(page_params)
        @page.save
        redirect_to @page, notice: 'Page was successfully created.'
    end

    #PATCH/PUT /page/1
    #PATCH/PUT /page/1.json
    respond_to :html, :json
    def update
        @page = Page.friendly.find params[:id]

        respond_to do |format|
            if @page.update_attributes(page_params)
                format.html { redirect_to(@page, :notice => 'Page was successfully updated.') }
                format.json { respond_with_bip(@page) }
            else
                format.html { redirect_to @page, notice: 'There was a error trying to update the page.' }
                format.json { respond_with_bip(@page) }
            end
        end
    end

      # DELETE /links/1
      # DELETE /links/1.json
      def destroy
       @page = Page.friendly.find(params[:id])
       @page.destroy
        respond_to do |format|
          format.html { redirect_to pages_path, notice: 'Page was successfully destroyed.' }
          format.json { head :no_content }
       end


end

private
  # Use callbacks to share common setup or constraints between actions.
  def set_pages
      @page = Page.friendly.find(params[:id])
  end

    # Never trust parameters from the scary internet, only allow the white list through.
    def page_params
      params.require(:page).permit(:title, :body)
  end


end
