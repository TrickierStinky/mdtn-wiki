require 'redcarpet'

class Page < ActiveRecord::Base
    extend FriendlyId
    friendly_id :title, use: :slugged

    def should_generate_new_friendly_id?
        new_record?
    end

    def redcarpet_render
        markdown = Redcarpet::Markdown.new(Redcarpet::Render::HTML, autolink: true, fenced_code_blocks: true, filter_html: true, hard_wrap: true, no_intraemphasis: true, gh_blockcode: true, strikethrough: true, highlight: true, )
        return SyntaxHighlighter.new(markdown.render(self.body)).hightlight_html.html_safe
    end

end


class SyntaxHighlighter
  require 'net/http'
  require 'uri'
  require 'nokogiri'

  PYGMENTS_URI = 'http://pygments.simplabs.com/'

  def initialize(html)
    @html = html
  end

  def hightlight_html
    doc = Nokogiri::HTML(@html)
    doc.search('pre > code[class]').each do |code|
      request = Net::HTTP.post_form(URI.parse(PYGMENTS_URI),
                  {'lang' => code[:class],
                   'code' => code.text.rstrip})

      code.parent.replace request.body
    end
    out = Nokogiri::HTML.fragment(doc.to_s)
    out.to_html
  end
end
